const responsive = true;
const Type = {
	DOU: "doughnut",
	BAR: "bar",
};
class Table {
	data = [];
	tbody = null;
	doughnut = null;
	bar = null;
	pages = 0;
	page = 0;
	limit = 10;
	constructor() {
		this.addListeners();
		this.tbody = $("table > tbody");
	}
	addListeners() {
		$("#file").change(this.handleUpload);
		$(".prev").click(() => this.prev());
		$(".next").click(() => this.next());
	}
	/**
	 * UI next button
	 */
	next() {
		if (this.page + 1 >= this.pages) {
			return;
		}
		this.page++;
		this.processData();
	}
	/**
	 * UI prev button
	 */
	prev() {
		if (this.page - 1 <= 0) {
			return;
		}
		this.page--;
		this.processData();
	}
	/**
	 * @function handleUpload upload JSON file
	 */
	handleUpload({ target }) {
		const reader = new FileReader();
		reader.readAsText(target.files[0]);
		reader.onload = ({ target }) => table.addData(target.result);
	}
	/**
	 * @function addData parse uploaded data
	 * @param {string} data
	 */
	addData(data) {
		try {
			const parsed = JSON.parse(data);
			this.data = parsed.sort((a, b) => a.name.localeCompare(b.name));
			this.pages = Math.round(this.data.length / this.limit);
			this.processData();
			this.showUi();
		} catch (e) {
			console.log(e);
			void e;
		}
	}
	/**
	 * update table & charts
	 */
	processData() {
		const data = this.sliceData();
		this.buildTable(data);
		this.updateCharts(data);
	}
	/**
	 * @function buildTable append rows to tbody
	 * @param {Array} this.data
	 */
	buildTable(rows) {
		this.tbody.html("");
		for (const row of rows) {
			const population = this.format(row.population);
			const vehicle = this.format(row.vehicle);
			const tr = `
        <tr>
          <td>${row.name}</td>
          <td>${population}</td>
          <td>${vehicle}</td>
        </tr>
      `;
			this.tbody.append(tr);
		}
	}
	/**
	 * @function updateCharts update OR create chart
	 * @param {Array} this.data
	 */
	updateCharts(data) {
		this.updateDoughnut(data);
		this.updateBar(data);
	}
	sliceData() {
		const skip = this.page ? this.page * this.limit : 0;
		const limit = skip + this.limit;
		return this.data.slice(skip, limit);
	}
	/**
	 * chart helpers
	 */
	doughnutChartData(data) {
		return {
			labels: data.map((d) => d.name),
			datasets: [
				{
					data: data.map((d) => d.population),
				},
			],
		};
	}
	barChartData(data) {
		return {
			labels: data.map((d) => d.name),
			datasets: [
				{
					data: data.map(({ vehicle, population }) =>
						this.diff(vehicle, population)
					),
				},
			],
		};
	}
	/**
	 * chart builder
	 */
	build(data, type) {
		const opt = {
			responsive,
			type,
			data,
		};
		this[type] = new Chart(type, opt);
	}
	/**
	 * chart updates
	 */
	updateDoughnut(data) {
		const { labels, datasets } = this.doughnutChartData(data);
		if (!this.doughnut) {
			this.build({ labels, datasets }, Type.DOU);
			return;
		}
		this.doughnut.data = { labels, datasets };
		this.doughnut.update();
	}
	updateBar(data) {
		const { labels, datasets } = this.barChartData(data);
		if (!this.bar) {
			this.build({ labels, datasets }, Type.BAR);
			return;
		}
		this.bar.data = { labels, datasets };
		this.bar.update();
	}
	/**
	 * helpers
	 */
	showUi() {
		try {
			$("table").removeClass("-hide");
			$(".buttons").removeClass("-hide");
			$(".chart").removeClass("-hide");
			$(".file").hide();
		} catch (e) {
			void e;
		}
	}
	format(n) {
		return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	}
	diff(a, b) {
		return parseFloat((a / b).toFixed(2));
	}
}
let table = null;
(function () {
	table = new Table();
})();
