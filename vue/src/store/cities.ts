import { defineStore } from 'pinia';
import { defaultState, defaultCity } from './cities.defaults';
import {
  KEY,
  CitiesState,
  SetParams,
  Pagination,
  CitiesActionNames as CAN,
} from './cities.interface';

export const useCitiesStore = defineStore(KEY, {
  state: (): CitiesState => defaultState(),
  getters: {
    pagination: (state: CitiesState): Pagination => ({
      total: state.total,
      current: state.page,
      pageSize: state.limit,
    }),
  },
  actions: {
    async [CAN.GET_CITIES](): Promise<void> {
      this.loading = true;
      const api = this.api.cities[CAN.GET_CITIES];
      const { skip, limit } = this;
      const { data } = await api({ skip, limit });
      this.cities = data.data;
      this.total = data.total;
      this.loading = false;
    },
    async [CAN.ADD_CITY](): Promise<void> {
      this.loadingModal = true;
      const api = this.api.cities[CAN.ADD_CITY];
      const { data } = await api(this.city);
      this.cities.unshift(data.data);
      this.city = defaultCity();
      this.loadingModal = false;
      this.modal = false;
      this.total++;
    },
    [CAN.SET_PARAMS]({ current, pageSize }: SetParams) {
      this.page = current;
      this.limit = pageSize;
      this.skip = (this.page - 1) * this.limit;
      this[CAN.GET_CITIES]();
    },
    [CAN.TOGGLE_MODAL]() {
      this.modal = !this.modal;
    },
  },
});
