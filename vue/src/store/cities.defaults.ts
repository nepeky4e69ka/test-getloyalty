import { CitiesState, City } from './cities.interface';

export const defaultState = (): CitiesState => ({
  cities: [],
  total: 0,
  page: 1,
  limit: 10,
  skip: 0,
  loading: false,
  loadingModal: false,
  modal: false,
  city: defaultCity(),
});

export const defaultCity = (): City => ({
  name: 'New',
  population: 0,
  vehicle: 0,
  display_name: 'New name',
});
