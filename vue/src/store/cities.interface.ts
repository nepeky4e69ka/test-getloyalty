export const KEY = 'cities';
export interface CitiesState {
  cities: City[];
  total: number;
  page: number;
  limit: number;
  skip: number;
  loading: boolean;
  modal: boolean;
  loadingModal: boolean;
  city: City;
}
export interface City {
  _id?: string;
  name: string;
  display_name: string;
  population: number;
  vehicle: number;
}

export interface SetParams {
  current: number;
  pageSize: number;
}
export interface Pagination {
  total: number;
  current: number;
  pageSize: number;
}

export enum CitiesActionNames {
  GET_CITIES = 'getCities',
  ADD_CITY = 'addCity',
  TOGGLE_MODAL = 'toggleModal',
  SET_PARAMS = 'setParams',
}
