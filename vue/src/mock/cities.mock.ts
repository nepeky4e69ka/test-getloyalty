import { MockFunction, MockHandler } from 'vite-plugin-mock-server';
import { City } from '@/store/cities.interface';
import { join } from 'path';
import { readFileSync } from 'fs';
import v4 from 'uuid4';
/**
 * helpers
 */
const pattern = '/api/cities';
const aSleep = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));
/**
 *
 * @returns {City}
 */
const createDb = () => {
  const path = join(__dirname, 'data.json');
  const data = readFileSync(path, 'utf8');
  const db = JSON.parse(data) as City[];
  return db
    .sort((a, b) => a.name.localeCompare(b.name))
    .map((a) => ({ ...a, _id: v4() }));
};
/**
 * Simulate DB
 */
const fakeDB: City[] = createDb();
/**
 * @function handleGet
 * @param req
 * @param res
 */
const handleGet: MockFunction = async (req, res) => {
  const skip = req.query?.skip ? parseInt(req.query?.skip) : 0;
  const limit = req.query?.limit ? parseInt(req.query?.limit) : 10;
  const end = limit + skip;
  const data = fakeDB.slice(skip, end);
  const total = fakeDB.length;
  const response = JSON.stringify({ data, total });
  // simulate res
  await aSleep(1000);
  res.setHeader('content-type', 'application/json; charset=utf-8');
  res.end(response);
};
/**
 * @function handlePost
 * @param req
 * @param res
 */
const handlePost: MockFunction = async (req, res) => {
  const _id = v4();
  const data = Object.assign(req.body, { _id });
  const response = JSON.stringify({ data });
  fakeDB.push(data);
  // simulate res
  await aSleep(1000);
  res.end(response);
};
/**
 * routes
 */
export default (): MockHandler[] => [
  {
    pattern,
    method: 'GET',
    handle: handleGet,
  },
  {
    pattern,
    method: 'POST',
    handle: handlePost,
  },
];
