import { axiosInstance as $axios } from './axios';
import { CitiesRepository, citiesRepository } from '@/api';

export type Repository = {
  cities: CitiesRepository;
};

const createRepository = (): Repository => {
  return {
    cities: citiesRepository($axios),
  };
};

export default createRepository();
