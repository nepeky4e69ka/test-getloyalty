import axios from 'axios';
import { config } from '@/utils';
export const axiosInstance = axios.create({
  baseURL: config.USER_API,
});
