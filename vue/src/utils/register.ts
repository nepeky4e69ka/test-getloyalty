import type { App } from 'vue';
import { createPinia } from 'pinia';
import { ComponentName } from './config';
import antDesign from 'ant-design-vue';
import api from '@/utils/repository';
import CitiesTable from '@/components/cities-table.vue';
import CitiesModal from '@/components/cities-modal.vue';
import CitiesActions from '@/components/cities-actions.vue';
/**
 * @param app
 * @returns { App }
 * @description register components & add plugins
 */
export const register = (app: App): App => {
  const pinia = createPinia();
  app.use(antDesign);
  app.use(pinia);
  // Inject http
  pinia.use(() => ({ api }));
  // Components
  app.component(ComponentName.CITIES_TABLE, CitiesTable);
  app.component(ComponentName.CITIES_MODAL, CitiesModal);
  app.component(ComponentName.CITIES_ACTIONS, CitiesActions);
  return app;
};
