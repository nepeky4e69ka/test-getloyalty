/**
 * @constant USER_API url provided from .env
 */
export const config = {
  USER_API: String(import.meta.env.VITE_USER_API) || '',
};
export const citiesForm = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
export const citiesColumns = [
  {
    title: 'Name',
    dataIndex: 'name',
  },
  {
    title: 'Population',
    dataIndex: 'population',
  },
  {
    title: 'Vehicle',
    dataIndex: 'vehicle',
  },
];
export enum ComponentName {
  CITIES_MODAL = 'CitiesModal',
  CITIES_TABLE = 'CitiesTable',
  CITIES_ACTIONS = 'CitiesActions',
}
