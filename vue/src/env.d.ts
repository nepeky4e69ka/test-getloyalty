/// <reference types="vite/client" />

/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

import 'pinia'
import { Repository } from '@/utils'
declare module 'pinia' {
  export interface PiniaCustomProperties {
    api: Repository
  }
}