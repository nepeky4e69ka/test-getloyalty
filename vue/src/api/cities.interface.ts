import { City } from '@/store/cities.interface';

export interface Params {
  skip: number;
  limit: number;
}
export interface GetResponse {
  total: number;
  data: City[];
}
export interface PostResponse {
  data: City;
}
export enum API {
  CITIES = '/cities',
}
