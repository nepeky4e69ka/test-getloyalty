import { City, CitiesActionNames as CAN } from '@/store/cities.interface';
import { AxiosInstance } from 'axios';
import { API, GetResponse, Params, PostResponse } from './cities.interface';

export const citiesRepository = (axios: AxiosInstance) => ({
  [CAN.GET_CITIES](params: Params) {
    return axios.get<GetResponse>(API.CITIES, { params });
  },
  [CAN.ADD_CITY](body: City) {
    return axios.post<PostResponse>(API.CITIES, body);
  },
});

export type CitiesRepository = ReturnType<typeof citiesRepository>;
