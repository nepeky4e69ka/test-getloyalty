import { createApp } from 'vue';
import { register } from './utils';
import App from '@/components/App.vue';
import './style/index.scss';
register(createApp(App)).mount('#app');
