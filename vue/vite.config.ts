import { defineConfig } from 'vite'
import { resolve } from 'path';
import { server } from './src/mock'
import vue from '@vitejs/plugin-vue'
export default defineConfig({
  plugins: [vue(), server],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },
  server: {
    open: true,
  },
})
